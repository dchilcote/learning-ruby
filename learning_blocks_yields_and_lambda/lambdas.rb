def lambda_demo(a_lambda)
  puts "I'm the method!"
  a_lambda.call
end

lambda_demo(lambda { puts "I'm the lambda!" })

#lambda { |param| block }


strings = ["leonardo", "donatello", "raphael", "michaelangelo"]
# Write your code below this line!


#initializing lambda right here

symbolize = lambda { |value| value.to_sym}

# Write your code above this line!
symbols = strings.collect(&symbolize)
print symbols



# Lambdas checks how many arguments are passed in where Procs do not


def batman_ironman_proc
  victor = Proc.new { return "Batman will win!" }
  victor.call
  "Iron Man will win!"
end

puts batman_ironman_proc


def batman_ironman_lambda
  victor = lambda { return "Batman will win!" }
  victor.call
  "Iron Man will win!"
end

puts batman_ironman_lambda


=begin
First, a lambda checks the number of arguments passed to it, while a proc does not. This means that a lambda will throw an error if you pass it the wrong number of arguments, whereas a proc will ignore unexpected arguments and assign nil to any that are missing.

Second, when a lambda returns, it passes control back to the calling method; when a proc returns, it does so immediately, without going back to the calling method.
=end




my_array = ["raindrops", :kettles, "whiskers", :mittens, :packages]

# Add your code below!
symbol_filter = lambda {|value| value.is_a? Symbol}

symbols = my_array.select(&symbol_filter)
puts symbols




=begin

A block is just a bit of code between do..end or {}. It’s not an object on its own, but it can be passed to methods like .each or .select.
A proc is a saved block we can use over and over.
A lambda is just like a proc, only it cares about the number of arguments it gets and it returns to its calling method rather than returning immediately.

=end



odds_n_ends = [:weezard, 42, "Trady Blix", 3, true, 19, 12.345]
#integer_filter = lambda { |value| value.is_a? Integer}
#ints = odds_n_ends.select(&integer_filter)
ints = odds_n_ends.select { |value| value.is_a? Integer}
puts ints



ages = [23, 101, 7, 104, 11, 94, 100, 121, 101, 70, 44]

# Add your code below!

#Check if variable is less than 100
under_100 = Proc.new {|value| value < 100}

#Run the Proc on each value in the array
youngsters = ages.select(&under_100)

puts youngsters



crew = {
  captain: "Picard",
  first_officer: "Riker",
  lt_cdr: "Data",
  lt: "Worf",
  ensign: "Ro",
  counselor: "Troi",
  chief_engineer: "LaForge",
  doctor: "Crusher"
}
# Add your code below!

first_half = lambda {|key, value| value < "M"}

a_to_m = crew.select(&first_half)

puts a_to_m





