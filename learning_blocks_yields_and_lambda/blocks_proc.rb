floats = [1.2, 3.45, 0.91, 7.727, 11.42, 482.911]
# Write your code below this line!
# .collect! and .map! are normally take blocks

round_down = Proc.new { |x| x.floor}

# Write your code above this line!
ints = floats.collect(&round_down)
print ints



#procs can be used over and over again, blocks cannot.


multiples_of_3 = Proc.new do |n|
  n % 3 == 0
end

print (1..100).to_a.select(&multiples_of_3)

# Here at the amusement park, you have to be four feet tall
# or taller to ride the roller coaster. Let's use .select on
# each group to get only the ones four feet tall or taller.

group_1 = [4.1, 5.5, 3.2, 3.3, 6.1, 3.9, 4.7]
group_2 = [7.0, 3.8, 6.2, 6.1, 4.4, 4.9, 3.0]
group_3 = [5.5, 5.1, 3.9, 4.3, 4.9, 3.2, 3.2]

# Complete this as a new Proc
over_4_feet = Proc.new {|x| x >= 4 }

# Change these three so that they use your new over_4_feet Proc
can_ride_1 = group_1.select(&over_4_feet)
can_ride_2 = group_2.select(&over_4_feet)
can_ride_3 = group_3.select(&over_4_feet)

puts can_ride_1
puts can_ride_2
puts can_ride_3




#define the greeter method
def greeter 
yield
end

#create proc
phrase = Proc.new do
puts "Hello there!"
end

#pass in proc using &procName
greeter(&phrase)


#you can call a proc with .call
hi = Proc.new {puts "Hello!"}
hi.call



numbers_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# Method names can be passed as symbols, and symbols can be converted to procs!
strings_array = numbers_array.map(&:to_s)

puts strings_array





