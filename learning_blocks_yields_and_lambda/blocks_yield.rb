def double(variable)
puts "before yield"
yield(variable)
puts "after yield"
end

double(2) {|x| puts x * 2}
