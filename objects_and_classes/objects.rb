class Person
  def initialize(name, age, profession)
    @name = name
    @age = age
    @profession = profession
  end
end


class Person
  # Set your class variable to 0 on line 3
  @@people_count = 0
  
  def initialize(name)
    @name = name
    # Increment your class variable on line 8
    @@people_count += 1
  end
  
  def self.number_of_instances
    # Return your class variable on line 13
    return @@people_count
  end
end

matz = Person.new("Yukihiro")
dhh = Person.new("David")

puts "Number of Person instances: #{Person.number_of_instances}"



class ApplicationError
  def display_error
    puts "Error! Error!"
  end
end

class SuperBadError < ApplicationError
end

err = SuperBadError.new
err.display_error



class Creature
  def initialize(name)
    @name = name
  end
  
  def fight
    return "Punch to the chops!"
  end
end

# Add your code below!
class Dragon < Creature
  def fight
  return "Breathes fire!"
  end
end





class Message
@@messages_sent = 0
def initialize(from, to)
@@messages_sent += 1
@from = from
@to = to
end
end

class Email < Message
def initialize(from, to)
super
end
end
my_message = Message.new("Jay", "Bob")







