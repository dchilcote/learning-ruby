symbol_hash = {
  :one => 1,
  :two => 2,
  :taco => "yummy"
}

puts :one
puts :two
puts :taco



strings = ["HTML", "CSS", "JavaScript", "Python", "Ruby"]

# Add your code below!

symbols = []
strings.each do |s| 
symbols.push(s.to_sym)
end
puts symbols


# .intern is the same .to_sym

strings = ["HTML", "CSS", "JavaScript", "Python", "Ruby"]

# Add your code below!

symbols = []
strings.each do |s| 
symbols.push(s.intern)
end
puts symbols
