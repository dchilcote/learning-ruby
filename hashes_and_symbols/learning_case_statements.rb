 movies = {"Lord of the Rings": 4}
 puts "Choose a command. add, update, display, delete"
 choice = gets.chomp

 case choice
 when "add"
	   puts "Give me a movie title:"
	     title = gets.chomp
	       if movies[title.to_sym] == nil
		           puts "Give me a rating between 1-4"
			       rating = gets.chomp
			           movies[title.to_sym] = rating.to_i
				     else
					         puts "That movie already exists!"
						   end
 when "update"
	   puts "Give the me title you want to update:"
	     title = gets.chomp
	      if movies[title.to_sym] == nil
		        puts "That movie doesn't exist!"
	      else
		        puts "Give me a rating:"
			  rating = gets.chomp
			    movies[title.to_sym] = rating.to_i
	      end
 when "display"
	   movies.each{ |name, score| puts "#{name}: #{score}"}
 when "delete"
	   puts "Give me a title:"
	     title = gets.chomp
	       if movies[title.to_sym] == nil
		           puts "That movie doesn't exist!"
	       else
		         movies.delete(title.to_sym)
	       end
 else
	   puts "Error!"
 end

